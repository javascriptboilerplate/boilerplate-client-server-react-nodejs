# boilerplate-client-server-react-nodejs

## Description

Minimal boilerplate for [ReactJs](https://reactjs.org/) client development using [Webpack](https://webpack.js.org/), [Babel](https://babeljs.io/) and [NodeJs](https://nodejs.org/en/) for server development.

Also contain [ESLint](https://eslint.org/) configured for client and server.

## Recommanded tools

1. [Visual Studio Code](https://code.visualstudio.com/)
    1. EditorConfig for VS Code
    2. ESLint (Dirk Baeumer)
    3. REST Client (Huachao Mao)

Note: Add the following settings in Visual Studio code to enable ESLint
```
  "eslint.autoFixOnSave": true,
  "javascript.format.enable": false,
  "editor.formatOnSave": true,
```

## Usage

1. For server development: `npm run dev-server`

    1. Start the server for development and automatically restart after code update.

2. For server development testing, use the file **test.rest** with the plugin **REST Client (Huachao Mao)**

    1. Each request can be execute to simulate a client request by clicking the label **"Send Request"** over each request separated by **"###"**

3. For client development: `npm run dev-client`

    1. Start the client for development and automatically reload after code update.

4. For deployment build: `npm run build`

    1. Produce a complete client application packaging ready for deployment in the **/dist** folder.

5. For deployment start: `npm start`

    1. Execute a complete client packaging and then start the server