/**
 * Central configuration for each deployment environment.
 * Note: See the header "Access-Control-Allow-Origin" configuration in server.js
 */

const prod = {
    server: 'http://localhost',
    port: 80
}

const dev = {
    server: 'http://localhost',
    port: 8080
}

const Environment = process.env.NODE_ENV === 'development' ? dev : prod

module.exports = Environment
