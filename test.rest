
GET http://localhost:8080/users HTTP/1.1

###

GET http://localhost:8080/users/1 HTTP/1.1

###

POST http://localhost:8080/users HTTP/1.1
content-type: application/x-www-form-urlencoded

name=chaloupe&age=45&id=666

###

POST http://localhost:8080/users HTTP/1.1
content-type: application/json

{ "name": "bateau", "age": 99, "id": 777 }

###

PUT http://localhost:8080/users HTTP/1.1
content-type: application/json

{ "id": 777, "userName": "bateau2", "age": 66 }

###

DELETE http://localhost:8080/users/666 HTTP/1.1