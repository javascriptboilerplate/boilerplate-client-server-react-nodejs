const HtmlWebPackPlugin = require('html-webpack-plugin')

module.exports = {
    resolve: {
        extensions: ['.jsx', '.js', '.json']
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.html$/,
                use: [{ loader: 'html-loader', options: { minimize: false } }]
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: 'src/index.html',
            filename: './index.html'
        })
    ]
}
