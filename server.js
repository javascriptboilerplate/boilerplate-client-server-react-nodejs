/**
 * If you install the plugin Rest Client, you can use the file test.rest to execute a
 * client request for each of the following endpoint.
 * In the file test.rest you will see the label 'Send Request' over each request,
 * click this label to execute the request.
 */

const Environment = require('./environment')

const express = require('express')
const bodyParser = require('body-parser')

const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

// Hosting for client project
app.use(express.static('dist'))

/**
 * In the server we use port 8080 and webpack-dev-derver will automatically use the following port number wich is 8081
 * The same origin policy will prevent the communication between the client and the server.
 * The following middleware add header to enable the communication for the specific development URL
 *
 * https://enable-cors.org/server_expressjs.html
 */
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8081')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

const CONTENT_TYPE_JSON = 'application/json'
const CONTENT_TYPE_HTML = 'text/html'
const HTTP_OK = 200

// Sample data hardcoded for example only
const USERS = [
    { name: 'Martin', age: '46', id: 0 },
    { name: 'Patrick', age: '76', id: 1 },
    { name: 'Mélanie', age: '41', id: 2 }
]

app.get('/', function (request, response) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
    response.end('<h1>Home page</h1>')
})

app.get('/users', function (request, response) {
    writeJSONResponse(request, response, USERS)
})

app.get('/users/:id', function (request, response) {
    const id = parseInt(request.params.id)
    writeJSONResponse(request, response, USERS.filter((user) => user.id === id))
})

app.post('/users', function (request, response) {
    // Return the complete request as response for test only
    writeJSONResponse(request, response, request.body)
})

app.put('/users', function (request, response) {
    // Return the complete request as response for test only
    writeJSONResponse(request, response, request.body)
})

app.delete('/users/:id', function (request, response) {
    // Return the specified id parameter as response for test only
    writeJSONResponse(request, response, request.params.id)
})

function writeJSONResponse (request, response, result) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(JSON.stringify(result, null, 2))
}

app.listen(Environment.port, function () {
    console.log('Server listening on: http://localhost:%s', Environment.port)
})
